package inf226.inchat;
import inf226.storage.Stored;

import java.security.Permission;
import java.time.Instant;

/**
 * The User class holds the public information
 * about a user.
 **/
public final class User {
    public final String name;
    public final Instant joined;

    public User(String name,
                Instant joined) {
        this.name = name;
        this.joined = joined;
    }
    
    /**
     * Create a new user.
     */
    public static User create(String name) {
        return new User(name, Instant.now());
    }


    //public Stored<User> setRole(Stored<User> user, Stored<Channel> channel, Stored<Permission> permission){
       /* Metode som setter de ulike rollene til en bruker.
        *  1. Owner: Set role, delete, remove, read and post (any message)
        *  2. Moderator: delete, edit, remove, read and post (any message)
        *  3. Participant: delete, edit, remove and post (own messages)
        *  4. Observer: Read
        *  5. Banned: No access to channel
        **/
/*
        if(permission.equals("owner"){
            true;
        }
        if(permission.equals("banned")) {
            false;
        }
        if(permission.equals("participant")) {
            MID = get(messageID)
            UserUUID = get(session.account.identity); //get id of curent logged in user
            if MID == UID {
                messageContent.update
            }
        if(permission.equals("observer")){

        }
        if(permission.equals("moderator")){

        }
    }

 */



}