package inf226.inchat;


import java.io.*;

import com.lambdaworks.crypto.SCryptUtil;

//This class creates password hashes using certain parameters
public class Password {

    //returns True if pw length is between 8 and 1000
    public static Boolean checkLength(String preHash){
        return (1000 > preHash.length() && preHash.length() > 7);
    }

    //returns True if pw is not found in our dictionary (cirt-default-passwords.txt)
    public static Boolean checkPwNotInDictionary(String preHash) throws IOException {
        File f1= new File("cirt-default-passwords.txt"); //Creation of File Descriptor for input file
        String[] words=null;  //Intialize the word Array
        FileReader fr = new FileReader(f1);  //Creation of File Reader object
        BufferedReader br = new BufferedReader(fr); //Creation of BufferedReader object
        String s;
        String input=preHash;   // Input word to be searched
        int count=0;   //Intialize the word to zero
        while((s=br.readLine())!=null)   //Reading Content from the file
        {
            words=s.split(" ");  //Split the word using space
            for (String word : words)
            {
                if (word.equalsIgnoreCase(input))   //Search for the given word
                {
                    count++;    //If Present increase the count by one
                }
            }
        }
        if(count!=0)  //Check for count not equal to zero
        {
            System.out.println("The given word is present for "+count+ " Times in the file");
            fr.close();
            return false;
        }
        else
        {
            System.out.println("The given word is not present in the file");
            fr.close();
            return true;
        }

    }

    //takes pw input by user, returns a hashed version
    public static String pwHasher(String preHash){
        String pwHashed = SCryptUtil.scrypt(preHash, 16384, 8, 1 );
        return pwHashed;
    }

    //takes preHash input-pw and postHash database-pw, returns True if correct match
    public static Boolean pwCheckComparedToHash(String preHash, String postHash){
        return (SCryptUtil.check(preHash, postHash));
    }


}
